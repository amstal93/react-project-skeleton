import * as React from "react";
import { shallow } from "enzyme";
import App from "../App";

describe("<App />", () => {
  it("renders App component with hello world message", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.text()).toEqual("Hello world React TS!");
  });
});
